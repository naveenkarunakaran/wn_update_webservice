﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WhereNetAPI;
using WNUpdateClientService.BusinessLogic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Configuration;

namespace WNUpdateClientService
{
    public class WNUpdateClient
    {
        private WNCOMApi theAPI;
        private object session = null;
        private string hostname;
        private string username;
        private string password;
        private string warehouseref;
        private string status = "%";
        private string processStatus = "%";
        private string commodity = "%";
        private string cartonCnt = "%";
        private string dc = "%";
        private string sealno = "%";
        private string bol = "%";
        private string pronum = "%";
        private string unitid = "";
        BLLWNUpdateClientService bLLWhereNetService = null;
        private string sdata = "";
        int nService_History_ID = 0;
        string BPID = "";
        APIResponce isupdateFailed = APIResponce.Success;
        Boolean isBPCall = true;

        private string Carrier = "%";
        private string Hold = "%";
        private string Priority = "%";

        private string ocean_scac = null;
        private string carrier_scac = null;
        private string terminal_name = null;
        private string vessel_name = null;
        private string voyage = null;
        private string unitcarrier = null;
        private DateTime vessel_eta;

        private string newvessel_name = null;
        private string newvoyage = null;

        //WN Update method
        public string UpdateExecutorData(string strargs, string sBPID, int? Service_History_ID, string Type)
        {
            try
            {
                if (Service_History_ID != null)
                {
                    isBPCall = false;
                }
                WriteLog("Entered Updateclient With Args : " + strargs + " | BPID : " + sBPID, EventLogEntryType.Information);
                BPID = sBPID;
                sdata = strargs;

                bLLWhereNetService = new BLLWNUpdateClientService();
                try
                {
                    if (isBPCall)
                    {
                        //Insert the WhereNet Service History log.
                        nService_History_ID = bLLWhereNetService.InsertLog(sdata, "", (int)EDIUpdtaeStatus.Reached_Update_Executor_Method, "Entered UpdateExecutor method with parameter '" + sdata + " ' and BPID as '" + BPID + "'", BPID);
                        WriteLog("Insertion of DB Service History log Successsful", EventLogEntryType.SuccessAudit);
                    }
                    else
                    {
                        nService_History_ID = Convert.ToInt32(Service_History_ID);
                    }
                }
                catch (Exception ex)
                {
                    WriteLog(ex.Message, EventLogEntryType.Error);
                }

                DataTable dt = null;

            NextEvent:
                WriteLog("Splitting Data Started", EventLogEntryType.Information);

                Dictionary<string, string> Args = new Dictionary<string, string>();

                //Getting Parameters in Dictionary
                foreach (var item in sdata.Split('|'))
                {
                    //When Key value pair is not available matching word to get value
                    if (item.Contains("WMPRD"))
                    {
                        Args.Add("WMPRD", item);
                    }
                    //When Key value pair is not available getting value
                    else if (!item.Contains("="))
                    {
                        Args.Add(item, null);
                    }
                    else
                    {
                        Args.Add(item.Split('=')[0].Trim(), item.Split('=')[1].Trim());
                    }
                }

                this.theAPI = new WhereNetAPI.WNCOMApiClass();

                //Looping arguments(Key Value pair) to get exact values based on name
                foreach (var item in Args)
                {
                    string ArgValue = Args.Where(q => q.Key == item.Key).Select(x => x.Value).FirstOrDefault();

                    switch (item.Key)
                    {

                        case "loadstatus":
                            this.status = ArgValue;
                            break;

                        case "processStatus":
                            this.processStatus = ArgValue;
                            break;

                        case "commodity":
                            this.commodity = ArgValue;
                            break;

                        case "cartoncnt":
                            this.cartonCnt = ArgValue;
                            break;

                        case "dcnumber":
                            this.dc = ArgValue;
                            break;

                        case "sealnumber":
                            this.sealno = ArgValue;
                            break;

                        case "pronum":
                            this.pronum = ArgValue;
                            break;

                        case "bol":
                            this.bol = ArgValue;
                            break;

                        case "id":
                            this.unitid = ArgValue;
                            break;

                        case "Priority":
                            this.Priority = ArgValue;
                            break;

                        case "Carrier":
                            this.Carrier = ArgValue;
                            break;

                        case "Hold":
                            this.Hold = ArgValue;
                            break;

                        case "WMPRD":
                            this.warehouseref = ArgValue;
                            break;

                        case "ocean_scac":
                            this.ocean_scac = ArgValue;
                            break;

                        case "terminal_name":
                            this.terminal_name = ArgValue;
                            break;

                        case "vessel_name":
                            this.vessel_name = ArgValue;
                            break;

                        case "voyage":
                            this.voyage = ArgValue;
                            break;

                        case "unitcarrier":
                            this.unitcarrier = ArgValue;
                            break;

                        case "carrier_scac":
                            this.carrier_scac = ArgValue;
                            break;

                        case "vessel_ETA":
                            this.vessel_eta = Convert.ToDateTime(ArgValue);
                            break;

                        //case "newvessel_name":
                        //    this.newvessel_name = ArgValue;
                        //    break;

                        //case "newvoyage":
                        //    this.newvoyage = ArgValue;
                        //    break;

                        default:
                            this.status = this.status + " " + item.Key;
                            break;
                    }
                }

                WriteLog("Splitting Data Completed", EventLogEntryType.Information);
                if (isBPCall)
                {
                    //Update the WhereNet Service History by splitted data
                    bLLWhereNetService.UpdateSplitData(nService_History_ID, processStatus, unitid);
                    WriteLog("Updation of split data in DB Successsful", EventLogEntryType.SuccessAudit);
                }
                //Only for Update Client Service With Type, Update using Stored procedures
                if (!string.IsNullOrEmpty(Type))
                {
                    string stypereturnmsg = "";
                    try
                    {
                        switch (Type)
                        {
                            case "OSCAC":
                                bLLWhereNetService.UpdateOceanScac(unitid, ocean_scac, cartonCnt, unitcarrier, vessel_eta);
                                bLLWhereNetService.UpdateCarrierScacForErrorEvent(unitid, ocean_scac, carrier_scac);
                                stypereturnmsg = "Updated Ocean Scac using Procedure";
                                break;
                            case "TERNL":
                                bLLWhereNetService.UpdateTerminal(terminal_name, vessel_name, voyage);
                                stypereturnmsg = "Updated Terminal using Procedure";
                                break;
                            //case "QUNTY":
                            //    bLLWhereNetService.UpdateQuantity(unitid, cartonCnt);
                            //    stypereturnmsg = "Updated Quantity using Procedure";
                            //    break;
                            default:
                                stypereturnmsg = "Not a valid Type";
                                break;
                        }
                        //Update the WhereNet Service History log about WNUpdate with Type
                        bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.UpdateSuccessful, "UPDATE with Type : " + stypereturnmsg, nService_History_ID, "NA");
                    }
                    catch (Exception ex)
                    {
                        stypereturnmsg = ex.Message;
                        bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.UpdateFailed, "UPDATE with Type : " + stypereturnmsg + " Failed", nService_History_ID, "NA");
                    }
                    return stypereturnmsg;
                }
                WriteLog("Fetching the previous processing flow status", EventLogEntryType.Information);
                //Fetching the processing flow status
                string sFromStatus = bLLWhereNetService.FetchFromStatus(processStatus);
                bool bHasEventQuePossibility = false;
                if (!string.IsNullOrEmpty(sFromStatus))
                {
                    WriteLog("previous processing flow status : " + sFromStatus, EventLogEntryType.Information);
                    WriteLog("checking whether the previous event had completed in WhereNet service history status ", EventLogEntryType.Information);
                    //check whether the previous event had completed in WhereNet service history status 
                    if (!bLLWhereNetService.DoesPrevEventCompleted(sFromStatus, unitid))
                    {
                        WriteLog("Previous Event Not Completed and Inserting in to WhereNet Service history queue", EventLogEntryType.Information);
                        //Insert the WhereNet Service history queue 
                        bLLWhereNetService.InsertWhereNetServiceLogQueue(sdata, BPID, processStatus, unitid, nService_History_ID);
                        WriteLog("WhereNet Service history queue Updated", EventLogEntryType.SuccessAudit);
                        return "Previous Event Not Completed";
                    }
                }
                else
                {
                    WriteLog("No previous processing flow status available ", EventLogEntryType.Information);
                    bHasEventQuePossibility = true;
                }

                WriteLog("Get Properties Started", EventLogEntryType.Information);
                this.GetProperties();
                WriteLog("Get Properties Completed", EventLogEntryType.Information);
                WriteLog("process Update Started", EventLogEntryType.Information);
                this.processUpdate();
                WriteLog("Exited processUpdate Method", EventLogEntryType.Information);
                string sreturnmsg = "";
                switch (isupdateFailed)
                {
                    case APIResponce.Error:
                        sreturnmsg = "Update Failed. Please view the DB log [or] Eventviewer for more info";
                        break;
                    case APIResponce.Success:
                        sreturnmsg = "Update Successful";
                        break;
                    case APIResponce.SuccessWorkAround:
                        sreturnmsg = "Update Successful With workaround";
                        break;
                    default:
                        break;
                }
                WriteLog(sreturnmsg, EventLogEntryType.Information);
                isBPCall = true;
                // If Event Queue Possibility is there then fetch the previous event and process it 
                if (bHasEventQuePossibility)
                {
                    WriteLog("Has Event QuePossibility", EventLogEntryType.Information);
                    //Fetching the WhereNet previous events in queue
                    WriteLog("Fetching Previous Event Queue for :" + processStatus, EventLogEntryType.Information);
                    DataTable dtcurrent = bLLWhereNetService.FetchPreviousEventQueue(processStatus, unitid);
                    //Check if there is Previous event is there for a Event in queue 
                    if (dt == null)
                    {
                        dt = dtcurrent.Clone();
                        foreach (DataRow dr in dtcurrent.Rows)
                        {
                            dt.Rows.Add(dr.ItemArray);
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in dtcurrent.Rows)
                        {
                            dt.Rows.Add(dr.ItemArray);
                        }
                    }
                    //Check if any event is in queue and process it 
                    if (dt != null && dt.Rows.Count != 0)
                    {
                        WriteLog("Previous Event Found in Queue 'Service_History_ID':" + nService_History_ID, EventLogEntryType.Information);
                        BPID = Convert.ToString(dt.Rows[0]["BPID"]);
                        sdata = Convert.ToString(dt.Rows[0]["Message_Input"]);
                        nService_History_ID = Convert.ToInt32(dt.Rows[0]["Service_History_ID"]);
                        WriteLog("Once Again Calling the Same Methods With the Fetched Queue Data", EventLogEntryType.Information);
                        dt.Rows.RemoveAt(0);
                        goto NextEvent;
                    }
                }
                //If there are two or more previous Events in queue process one by one
                else if (dt != null && dt.Rows.Count != 0)
                {
                    WriteLog("Previous Event Found in Queue 'Service_History_ID':" + nService_History_ID, EventLogEntryType.Information);
                    BPID = Convert.ToString(dt.Rows[0]["BPID"]);
                    sdata = Convert.ToString(dt.Rows[0]["Message_Input"]);
                    nService_History_ID = Convert.ToInt32(dt.Rows[0]["Service_History_ID"]);
                    WriteLog("Once Again Calling the Same Methods With the Fetched Queue Data", EventLogEntryType.Information);
                    dt.Rows.RemoveAt(0);
                    goto NextEvent;
                }
                return sreturnmsg;
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message, EventLogEntryType.Error);
                return ex.Message;
            }
        }

        private bool processUpdate()
        {
            try
            {
                WNReturnVal wNReturnVal = this.theAPI.WNOpen(this.hostname, this.username, this.password, "", ref this.session);
                if (wNReturnVal != WNReturnVal.WNSUCCESS)
                {
                    try
                    {
                        WriteLog("Wherenet API Connection Opened Failed | Error: " + wNReturnVal.ToString(), EventLogEntryType.Error);
                        isupdateFailed = APIResponce.Error;
                        //Update the WhereNet Service History log
                        bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.ConnectionFailed, wNReturnVal.ToString(), nService_History_ID, Convert.ToString(this.session));
                        WriteLog("Updation of DB Service History log Successsful", EventLogEntryType.SuccessAudit);
                    }
                    catch (Exception ex)
                    {
                        WriteLog("UpdateLog Failed : " + ex.Message, EventLogEntryType.Error);
                    }
                    object obj = Convert.ToInt32(wNReturnVal);
                    this.theAPI.WNClose(ref this.session);
                    return false;
                }
                try
                {
                    WriteLog("Wherenet API Connection Opened Successful", EventLogEntryType.Information);
                    //Update the WhereNet Service History log
                    bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.ConnectionOpened, wNReturnVal.ToString(), nService_History_ID, Convert.ToString(this.session));
                    WriteLog("Updation of DB Service History log Successsful", EventLogEntryType.SuccessAudit);
                }
                catch (Exception ex)
                {
                    WriteLog("UpdateLog Failed : " + ex.Message, EventLogEntryType.Error);
                }
                string text = string.Concat(new string[]
                    {
                        "String = '",this.Carrier,"/f",
                        this.unitid,
                        "/f%/f%/f",this.cartonCnt,"/f",this.commodity,"/f%/f",
                        this.processStatus,
                        "/f",
                        this.status,
                        "/f%/f",this.Hold,"/f",
                        this.dc,
                        "/f",
                        this.bol,
                        "/f",
                        this.pronum,
                        "/f%/f",
                        this.sealno,
                        "/f",this.Priority,"/f%/f%/f'"
                    });

                WriteLog("Going to Update Wherenet with Data :" + text, EventLogEntryType.SuccessAudit);
                //Update WhereNet Status
                WNReturnVal wNReturnVal2 = this.theAPI.WNUpdate(ref this.session, "API Trailer Update", text);

                if (wNReturnVal2 == WNReturnVal.WNSUCCESS)
                {
                    try
                    {
                        if (!isBPCall)
                        {
                            bLLWhereNetService.InsertReprocessHistory(nService_History_ID, wNReturnVal2.ToString());
                        }
                        WriteLog("Wherenet API Updation Successful", EventLogEntryType.SuccessAudit);
                        //Update the WhereNet Service History log
                        bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.UpdateSuccessful, wNReturnVal2.ToString(), nService_History_ID, Convert.ToString(this.session));
                        WriteLog("Updation of DB Service History log Successsful", EventLogEntryType.SuccessAudit);
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message, EventLogEntryType.Error);
                    }
                    this.theAPI.WNClose(ref this.session);
                    return true;
                }
                WriteLog("Wherenet API Updation Failed with Error : " + wNReturnVal2.ToString(), EventLogEntryType.SuccessAudit);
                object obj2 = Convert.ToInt32(wNReturnVal2);
                string serr = this.theAPI.WNGetUpdateError(ref this.session, ref obj2);



                if (!isBPCall)
                {
                    bLLWhereNetService.InsertReprocessHistory(nService_History_ID, serr);
                }
                try
                {
                    isupdateFailed = APIResponce.Error;
                    //Update the WhereNet Service History log
                    bLLWhereNetService.UpdateLog((int)EDIUpdtaeStatus.UpdateFailed, wNReturnVal2.ToString() + " : (" + serr.ToString() + ")", nService_History_ID, Convert.ToString(this.session));
                    WriteLog("Updation of DB Service History log Successsful", EventLogEntryType.SuccessAudit);
                }
                catch (Exception ex)
                {
                    WriteLog(ex.Message, EventLogEntryType.Error);
                }
                this.theAPI.WNClose(ref this.session);
                return false;
            }
            catch (Exception)
            {
                throw;
            }

        }
        //Get Connection String details
        private void GetProperties()
        {
            try
            {
                StreamReader streamReader = new StreamReader(new FileStream(System.Web.HttpContext.Current.Server.MapPath("~/ClientProperties/client.properties"), FileMode.Open, FileAccess.Read));
                while (!streamReader.EndOfStream)
                {
                    string text = streamReader.ReadLine();
                    if (text.Contains(this.warehouseref))
                    {
                        string text2 = text.Substring(text.IndexOf(":"));
                        if (text2.Contains("servername"))
                        {
                            this.hostname = text2.Substring(text2.IndexOf("=") + 1, text2.IndexOf(",") - text2.IndexOf("=") - 1);
                            text2 = text2.Remove(1, text2.IndexOf(","));
                        }
                        if (text2.Contains("username"))
                        {
                            this.username = text2.Substring(text2.IndexOf("=") + 1, text2.IndexOf(",") - text2.IndexOf("=") - 1);
                            text2 = text2.Remove(1, text2.IndexOf(","));
                        }
                        if (text2.Contains("password"))
                        {
                            this.password = text2.Substring(text2.IndexOf("=") + 1);
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void WriteLog(string message, EventLogEntryType elogtype)
        {
            try
            {
                switch (elogtype)
                {
                    case EventLogEntryType.Error:
                        CLogger.WriteLog(ELogLevel.ERROR, message);
                        break;
                    case EventLogEntryType.FailureAudit:
                        CLogger.WriteLog(ELogLevel.ERROR, message);
                        break;
                    case EventLogEntryType.Information:
                        CLogger.WriteLog(ELogLevel.INFO, message);
                        break;
                    case EventLogEntryType.SuccessAudit:
                        CLogger.WriteLog(ELogLevel.INFO, message);
                        break;
                    case EventLogEntryType.Warning:
                        CLogger.WriteLog(ELogLevel.WARN, message);
                        break;
                    default:
                        CLogger.WriteLog(ELogLevel.FATAL, message);
                        break;
                }

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsEventLogNeeded"]))
                {
                    if (!EventLog.SourceExists("EDIWebService", ConfigurationManager.AppSettings["SystemName"]))
                    {
                        EventLog.CreateEventSource("EDIWebService", "EDIWebServiceLog");
                    }

                    EventLog myLog = new EventLog();
                    myLog.Source = "EDIWebService";
                    myLog.WriteEntry(message, elogtype);
                }
            }
            catch (Exception)
            {

            }
        }

        private enum EDIUpdtaeStatus
        {
            ConnectionOpened = 101,
            ConnectionFailed = 102,
            UpdateSuccessful = 103,
            UpdateFailed = 104,
            Reached_Update_Executor_Method = 105,
            Updated_Carrier_SCAC_Procedure = 106
        }


        private enum APIResponce
        {
            Error,
            Success,
            SuccessWorkAround
        }
    }
}