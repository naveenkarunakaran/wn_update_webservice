﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WNUpdateClientService
{
    public enum ELogLevel
    {
        DEBUG = 1,
        ERROR,
        FATAL,
        INFO,
        WARN
    }
}