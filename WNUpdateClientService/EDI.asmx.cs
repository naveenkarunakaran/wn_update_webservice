﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;
using WNUpdateClientService.BusinessLogic;

namespace WNUpdateClientService
{
    /// <summary>
    /// Summary description for EDI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EDI : System.Web.Services.WebService
    {

        /// <summary>
        /// Update WN Client service with type
        /// </summary>
        /// <param name="InputParameter"></param>
        /// <param name="BPID"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public string UpdateClientServiceWithType(String InputParameter, String BPID, String Type)
        {

            WNUpdateClient UE = new WNUpdateClient();
            //Calling the update client  function and return the string value status "Update Successful  or  Update Failed"
            return UE.UpdateExecutorData(InputParameter, BPID, null, Type);
        }


        /// <summary>
        /// Update WN Client service
        /// </summary>
        /// <param name="InputParameter"></param>
        /// <param name="BPID"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public string UpdateClientService(String InputParameter, String BPID)
        {

            WNUpdateClient UE = new WNUpdateClient();
            //Calling the update client  function and return the string value status "Update Successful  or  Update Failed"
            return UE.UpdateExecutorData(InputParameter, BPID, null, null);
        }


        /// <summary>
        /// Reprocess the Failed events
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public string ReProcessFailedEvents()
        {
            try
            {
                BLLWNUpdateClientService bLLWNService = new BLLWNUpdateClientService();
                DataTable dt = bLLWNService.FetchFailedEvents();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            WNUpdateClient UE = new WNUpdateClient();
                            //Calling the update client  function and return the string value status "Update Successful  or  Update Failed"
                            UE.UpdateExecutorData(Convert.ToString(dr["Message_Input"]), Convert.ToString(dr["BPID"]), Convert.ToInt32(dr["Service_History_ID"]), null);
                        }
                    }
                }
                return "Process Successful";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        /// <summary>
        /// Email Incomplete events (WhereNet Service history queue) to mentioned email address
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public string SendIncompeteEventsMail()
        {
            try
            {
                string smaildt = "No Events";
                BLLWNUpdateClientService bLLWNService = new BLLWNUpdateClientService();
                DataTable dt = bLLWNService.FetchNotificationHistory(Convert.ToInt32(ConfigurationManager.AppSettings["MinutesDelay"]));





                DataTable containerdt = new DataTable();

                containerdt.Columns.Add("Id", typeof(string));

                containerdt.Columns.Add("ProcessStatus", typeof(string));

                containerdt.Columns.Add("Service_History_Queue_ID", typeof(int));


                DataTable trailerdt = new DataTable();
                trailerdt.Columns.Add("Id", typeof(string));

                trailerdt.Columns.Add("ProcessStatus", typeof(string));

                trailerdt.Columns.Add("Service_History_Queue_ID", typeof(int));

                string[] stringArray = ConfigurationManager.AppSettings["ContainerEvents"].Split('|');
                bool isContainerEvent = false;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["Adena_Event"])))
                    {
                        foreach (string x in stringArray)
                        {

                            if (x.Trim().ToLower() == dr["Adena_Event"].ToString().Trim().ToLower())
                            {
                                isContainerEvent = true;
                                break;
                            }

                        }

                        if (isContainerEvent)
                        {
                            DataRow row = containerdt.NewRow();
                            row["Id"] = dr["Id"].ToString();
                            row["ProcessStatus"] = dr["ProcessStatus"].ToString();
                            row["Service_History_Queue_ID"] = dr["Service_History_Queue_ID"].ToString();
                            containerdt.Rows.Add(row);
                            isContainerEvent = false;
                        }
                        else
                        {
                            DataRow row = trailerdt.NewRow();
                            row["Id"] = dr["Id"].ToString();
                            row["ProcessStatus"] = dr["ProcessStatus"].ToString();
                            row["Service_History_Queue_ID"] = dr["Service_History_Queue_ID"].ToString();
                            trailerdt.Rows.Add(row);
                        }
                    }
                }

                if (containerdt != null)
                {
                    if (containerdt.Rows.Count > 0)
                    {
                        SendMail("Container", "Container Open", bLLWNService, containerdt);
                    }

                }

                if (trailerdt != null)
                {
                    if (trailerdt.Rows.Count > 0)
                    {
                        SendMail("Trailer", "Trailer Loading", bLLWNService, trailerdt);
                    }

                }
                smaildt = "Mail Sent for Container Events : '" + containerdt.Rows.Count + "' & Trailer Events : '" + trailerdt.Rows.Count + "'";
                return smaildt;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }


        /// <summary>
        /// Send mail code with email body format
        /// </summary>
        /// <param name="EventType"></param>
        /// <param name="PrevEvent"></param>
        /// <param name="bLLWNService"></param>
        /// <param name="dt"></param>
        private void SendMail(string EventType, string PrevEvent, BLLWNUpdateClientService bLLWNService, DataTable dt)
        {
            try
            {
                StringBuilder returnNumber = new StringBuilder();
                returnNumber.Append(EventType + "~");
                returnNumber.Append(PrevEvent + "~");
                returnNumber.Append(ConfigurationManager.AppSettings["MinutesDelay"] + "~");
                returnNumber.Append(@"<table width='100%'>");
                // returnNumber.Append(@"<tr><td>Id</td><td> Received Process Status</td></tr>");
                foreach (DataRow dr in dt.Rows)
                {
                    returnNumber.Append(@"<tr><td style='border-bottom:1px solid black'>" + dr["Id"] + "</td></tr>");
                }
                returnNumber.Append(@"</table>");

                bool EnableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                XmlNode xmlNode = GetXmlMailNode("PrevEventNotReceived");
                string sFormatString = xmlNode.InnerXml;
                sFormatString = string.Format(sFormatString, returnNumber.ToString().Split('~'));
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add(ConfigurationManager.AppSettings["ToMailAddress"]);
                mailMessage.IsBodyHtml = true;

                string strsubject = xmlNode.Attributes["subject"].Value;
                mailMessage.Subject = strsubject;
                mailMessage.Body = sFormatString;
                SmtpClient smtp = new SmtpClient();
                smtp.EnableSsl = EnableSSL;
                smtp.Send(mailMessage);
                foreach (DataRow dr in dt.Rows)
                {
                    bLLWNService.UpdateNotificationHistory(Convert.ToInt32(dr["Service_History_Queue_ID"]));
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Getting XML File for mail format to send
        /// </summary>
        /// <param name="sSectionName"></param>
        /// <returns></returns>
        public XmlNode GetXmlMailNode(string sSectionName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                string sPath = "";
                sPath = "~/XML/Mail.xml";
                xmlDoc.Load(Server.MapPath(sPath));
                XmlNodeList xmlParentNodeList = xmlDoc.DocumentElement.ChildNodes;
                foreach (XmlNode xmlChildNode in xmlParentNodeList)
                {
                    if (xmlChildNode.Attributes["Name"].Value == sSectionName)
                    {
                        return xmlChildNode;
                    }
                }
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
