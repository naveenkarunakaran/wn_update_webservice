﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WNUpdateClientService
{
    public static class CLogger
    {
        private static readonly ILog logger;
        static CLogger()
        {
            string str = DateTime.Now.ToString("MM_dd_yy");
            GlobalContext.Properties["LogName"] = "EdiWebService_" + str + ".log";
            CLogger.logger = LogManager.GetLogger(typeof(CLogger));
            XmlConfigurator.Configure();
        }
        public static void WriteLog(ELogLevel logLevel, string log)
        {
            if (logLevel.Equals(ELogLevel.DEBUG))
            {
                CLogger.logger.Debug(log);
            }
            else
            {
                if (logLevel.Equals(ELogLevel.ERROR))
                {
                    CLogger.logger.Error(log);
                }
                else
                {
                    if (logLevel.Equals(ELogLevel.FATAL))
                    {
                        CLogger.logger.Fatal(log);
                    }
                    else
                    {
                        if (logLevel.Equals(ELogLevel.INFO))
                        {
                            CLogger.logger.Info(log);
                        }
                        else
                        {
                            if (logLevel.Equals(ELogLevel.WARN))
                            {
                                CLogger.logger.Warn(log);
                            }
                        }
                    }
                }
            }
        }
    }
}