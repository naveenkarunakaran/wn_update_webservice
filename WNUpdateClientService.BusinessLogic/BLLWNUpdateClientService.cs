﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WNUpdateClientService.BusinessLogic
{
    public class BLLWNUpdateClientService
    {
        public string sSqlConnectionString = ConfigurationManager.ConnectionStrings["EDIWebServiceCSV"].ConnectionString;
        public string sSqlWherenetDBUpdateCS = ConfigurationManager.ConnectionStrings["WherenetDBUpdate"].ConnectionString;
        SqlConnection con = null;

        //Update the WhereNet Service History log
        public void UpdateLog(int Status_ID, string sComments, int Service_History_ID, string WhereNet_session_ID)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Update_WhereNet_Service_Log", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Status_ID", Status_ID));
                command.Parameters.Add(new SqlParameter("@Comments", sComments));
                command.Parameters.Add(new SqlParameter("@Service_History_ID", Service_History_ID));
                command.Parameters.Add(new SqlParameter("@WhereNet_session_ID", WhereNet_session_ID));

                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Update the WhereNet Service History by splitted data
        public void UpdateSplitData(int Service_History_ID, string ProcessStatus, string Id)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Update_Split_Data", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ProcessStatus", ProcessStatus));
                command.Parameters.Add(new SqlParameter("@Id", Id));
                command.Parameters.Add(new SqlParameter("@Service_History_ID", Service_History_ID));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Insert the WhereNet Service History log.
        public int InsertLog(string Message_Input, string WhereNet_session_ID, int Status_ID, string sComments, string sBPID)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Insert_WhereNet_Service_Log", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Message_Input", Message_Input));
                if (string.IsNullOrEmpty(WhereNet_session_ID))
                {
                    command.Parameters.Add(new SqlParameter("@WhereNet_session_ID", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@WhereNet_session_ID", WhereNet_session_ID));
                }
                command.Parameters.Add(new SqlParameter("@BPID", sBPID));
                command.Parameters.Add(new SqlParameter("@Status_ID", Status_ID));
                command.Parameters.Add(new SqlParameter("@Comments", sComments));
                command.Parameters.Add("@Service_History_ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                int nService_History_ID = Convert.ToInt32(command.Parameters["@Service_History_ID"].Value);
                return nService_History_ID;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Insert the WhereNet Service history queue 
        public int InsertWhereNetServiceLogQueue(string Message_Input, string sBPID, string ProcessStatus, string Id, int nService_History_ID)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Insert_WhereNet_Service_Log_Queue", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Message_Input", Message_Input));
                command.Parameters.Add(new SqlParameter("@BPID", sBPID));
                command.Parameters.Add(new SqlParameter("@ProcessStatus", ProcessStatus));
                command.Parameters.Add(new SqlParameter("@Id", Id));
                command.Parameters.Add(new SqlParameter("@Service_History_ID", nService_History_ID));
                command.Parameters.Add("@Service_History_Queue_ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                int nService_HistoryQue_ID = Convert.ToInt32(command.Parameters["@Service_History_Queue_ID"].Value);
                return nService_HistoryQue_ID;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        //Fetching the processing flow status
        public string FetchFromStatus(string sProcessStatus)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Fetch_From_Status", con);
                cmd.Parameters.Add(new SqlParameter("@Process_Status_To", sProcessStatus));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                con.Close();
                if (dt == null || dt.Rows.Count == 0)
                    return null;
                else
                    return Convert.ToString(dt.Rows[0]["Process_Status_From"]);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Fetching the WhereNet previous events in queue
        public DataTable FetchPreviousEventQueue(string sPrevProcessStatus, string Id)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Fetch_Previous_Event_Queue", con);
                cmd.Parameters.Add(new SqlParameter("@ProcessStatus", sPrevProcessStatus));
                cmd.Parameters.Add(new SqlParameter("@Id", Id));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                con.Close();

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Fetching the WhereNet Failed events
        public DataTable FetchFailedEvents()
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Fetch_Failed_Events", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                con.Close();

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //check whether the previous event had completed in WhereNet service history status 
        public bool DoesPrevEventCompleted(string sProcessStatus, string Id)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Does_Prev_Event_Completed", con);
                cmd.Parameters.Add(new SqlParameter("@ProcessStats", sProcessStatus));
                cmd.Parameters.Add(new SqlParameter("@Id", Id));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                con.Close();
                if (dt == null || dt.Rows.Count == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Insert the WhereNet Reprocess History
        public void InsertReprocessHistory(int Service_History_ID, string Comments)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Insert_Reprocess_History", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Service_History_ID", Service_History_ID));
                command.Parameters.Add(new SqlParameter("@Comments", Comments));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }


        //Fetch Notification Queue
        public DataTable FetchNotificationHistory(int minutes)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Fetch_Notification_History", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@dealayMinutes", minutes));
                SqlDataReader reader = command.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                con.Close();

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        // Fetch Notification Status
        public void UpdateNotificationHistory(int Service_History_Queue_ID)
        {
            try
            {
                con = new SqlConnection(sSqlConnectionString);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Updte_Notification_History_As_Notified", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Service_History_Queue_ID", Service_History_Queue_ID));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //Update Ocean SCAC with Procedure
        public void UpdateOceanScac(string EquipmentID, string ocean_scac, string Quantity, string unitcarrier, DateTime vessel_eta)
        {
            try
            {
                con = new SqlConnection(sSqlWherenetDBUpdateCS);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Update_Ocen_Scac", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@EquipmentID", EquipmentID));
                if (string.IsNullOrEmpty(ocean_scac))
                {
                    command.Parameters.Add(new SqlParameter("@ocean_scac", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@ocean_scac", ocean_scac));
                }
                if (string.IsNullOrEmpty(Quantity))
                {
                    command.Parameters.Add(new SqlParameter("@Quantity", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@Quantity", Quantity));
                }
                if (string.IsNullOrEmpty(unitcarrier))
                {
                    command.Parameters.Add(new SqlParameter("@unitcarrier", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@unitcarrier", unitcarrier));
                }
                if (string.IsNullOrEmpty(Convert.ToString(vessel_eta)))
                {
                    command.Parameters.Add(new SqlParameter("@los_start_date", DBNull.Value));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("@los_start_date", vessel_eta));
                }
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //public bool UpdateOceanScac(string EquipmentID, string ocean_scac, string Quantity, string unitcarrier, DateTime vessel_eta)
        //{
        //    try
        //    {
        //        con = new SqlConnection(sSqlWherenetDBUpdateCS);
        //        con.Open();
        //        SqlCommand command = new SqlCommand("SP_Update_Ocen_Scac", con);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@EquipmentID", EquipmentID));
        //        if (string.IsNullOrEmpty(ocean_scac))
        //        {
        //            command.Parameters.Add(new SqlParameter("@ocean_scac", DBNull.Value));
        //        }
        //        else
        //        {
        //            command.Parameters.Add(new SqlParameter("@ocean_scac", ocean_scac));
        //        }
        //        if (string.IsNullOrEmpty(Quantity))
        //        {
        //            command.Parameters.Add(new SqlParameter("@Quantity", DBNull.Value));
        //        }
        //        else
        //        {
        //            command.Parameters.Add(new SqlParameter("@Quantity", Quantity));
        //        }
        //        if (string.IsNullOrEmpty(unitcarrier))
        //        {
        //            command.Parameters.Add(new SqlParameter("@unitcarrier", DBNull.Value));
        //        }
        //        else
        //        {
        //            command.Parameters.Add(new SqlParameter("@unitcarrier", unitcarrier));
        //        }
        //        if (string.IsNullOrEmpty(Convert.ToString(vessel_eta)))
        //        {
        //            command.Parameters.Add(new SqlParameter("@los_start_date", DBNull.Value));
        //        }
        //        else
        //        {
        //            command.Parameters.Add(new SqlParameter("@los_start_date", vessel_eta));
        //        }
        //        command.Parameters.Add("@IsTrailerExists", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        command.ExecuteNonQuery();
        //        bool bIsTrailerExists = Convert.ToBoolean(command.Parameters["@IsTrailerExists"].Value);
        //        return bIsTrailerExists;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        //Update Terminal with Procedure
        public void UpdateTerminal(string terminal_name, string vessel_name, string voyage/*,string newvessel_name, string newvoyage,string ocean_scac, DateTime vessel_eta*/)
        {
            try
            {
                con = new SqlConnection(sSqlWherenetDBUpdateCS);
                con.Open();
                SqlCommand command = new SqlCommand("SP_Update_Terminal", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@terminal_name", terminal_name));
                command.Parameters.Add(new SqlParameter("@vessel_name", vessel_name));
                command.Parameters.Add(new SqlParameter("@voyage", voyage));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //public bool UpdateTerminal(string terminal_name, string vessel_name, string voyage, string newvessel_name, string newvoyage, string ocean_scac, DateTime vessel_eta)
        //{
        //    try
        //    {
        //        con = new SqlConnection(sSqlWherenetDBUpdateCS);
        //        con.Open();
        //        SqlCommand command = new SqlCommand("SP_Update_Terminal_New", con);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@terminal_name", terminal_name));
        //        command.Parameters.Add(new SqlParameter("@vessel_name", vessel_name));
        //        command.Parameters.Add(new SqlParameter("@voyage", voyage));
        //        command.Parameters.Add(new SqlParameter("@vessel_nameNew", newvessel_name));
        //        command.Parameters.Add(new SqlParameter("@voyageNew", newvoyage));
        //        command.Parameters.Add(new SqlParameter("@ocean_scac", ocean_scac));
        //        command.Parameters.Add(new SqlParameter("@vessel_eta", vessel_eta));
        //        command.Parameters.Add("@IsVesselVoyageExists", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        command.ExecuteNonQuery();
        //        bool bIsVesselVoyageExists = Convert.ToBoolean(command.Parameters["@IsVesselVoyageExists"].Value);
        //        return bIsVesselVoyageExists;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        //Update Carrier SCAC

        public void UpdateCarrierScacForErrorEvent(string unitid, string OceanSCAC, string CarrierSCAC)
        {
            try
            {
                con = new SqlConnection(sSqlWherenetDBUpdateCS);
                con.Open();
                SqlCommand command = new SqlCommand("USP_Update_CarrierScac_ErrorEvent", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@unitid", unitid));
                command.Parameters.Add(new SqlParameter("@OceanSCAC", OceanSCAC));
                command.Parameters.Add(new SqlParameter("@CarrierSCAC", CarrierSCAC));
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        //public bool UpdateCarrierScacForErrorEvent(string unitid, string OceanSCAC, string CarrierSCAC)
        //{
        //    try
        //    {
        //        con = new SqlConnection(sSqlWherenetDBUpdateCS);
        //        con.Open();
        //        SqlCommand command = new SqlCommand("USP_Update_CarrierScac_ErrorEvent", con);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@unitid", unitid));
        //        command.Parameters.Add(new SqlParameter("@OceanSCAC", OceanSCAC));
        //        command.Parameters.Add(new SqlParameter("@CarrierSCAC", CarrierSCAC));
        //        command.Parameters.Add("@IsTrailerExists", SqlDbType.Bit).Direction = ParameterDirection.Output;
        //        command.ExecuteNonQuery();
        //        bool bIsTrailerExists = Convert.ToBoolean(command.Parameters["@IsTrailerExists"].Value);
        //        return bIsTrailerExists;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

    }
}